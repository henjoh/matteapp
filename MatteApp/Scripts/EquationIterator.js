﻿var m_model;
var m_currentIndex;
var m_numItems;
var m_incorrectAnswers = [];
var m_started = false;
var m_startTime;
var m_timer;
var m_timerType = 0;
var m_timerLimit = 0;
var m_timeSpent;

//TODO:
//-extract css styles
//-track result (success on first try?) and show when done
//-take time and show timer
//-extract problems?

function initEquationIterator(model, rerun)
{
    m_model = model;
    m_currentIndex = 0;
    m_numItems = m_model.length;
    m_incorrectAnswers = [];
    m_started = rerun || false;

    displayEquationIterator();
}

function displayEquation()
{
    var counterDiv = document.getElementById("equationiteratorcounter");
    counterDiv.innerHTML = (m_currentIndex + 1).toString() + " av " + m_numItems.toString();

    var formulaLabel = document.getElementById("equationiteratorformula");
    formulaLabel.innerHTML = m_model[m_currentIndex].Formula;

    var resultInput = document.getElementById("equationresult");
    resultInput.value = "";
    resultInput.focus();
    resultInput.select();
    resultInput.addEventListener("blur", function(e) 
    {
        resultInput.focus();
    });

}

function displayNextEquation()
{
    m_currentIndex++;
    if (m_currentIndex < m_numItems) {
        displayEquation();
    }
    else {
        stopTimer();
        displayFinished();
    }
}

function displayFinished() {
    var div = document.getElementById("equationiterator");
    div.innerHTML = "<br/>Du klarade det på " + m_timeSpent + ". Bra jobbat!";
    if (m_incorrectAnswers.length > 0) {
        div.innerHTML += "<br/><font size=\"4\">Du gissade fel på " + m_incorrectAnswers.length.toString() + " tal." +
            "<br/><input type='button' value='Kör om de här' onClick='rerunIncorrect();'/>";
    }
    else {
        //TODO: knapp - kör igen
    }
    //TODO: knapp - till startsidan
}

function rerunIncorrect()
{
    var newModel = [];
    for (var i = 0; i < m_incorrectAnswers.length; i++) {
        newModel[i] = m_model[m_incorrectAnswers[i]];
    }
    initEquationIterator(newModel, true);
    displayEquation();
}

function displayEquationIterator()
{
    var div = document.getElementById("equationiterator");
    if (m_started === true) {
        div.style = "font-size: 25px;";
        var html = '<div id="equationiteratorcounter"></div> <div id="timer">...</div><br/>' +
            '<label id="equationiteratorformula"></label> = ' +
            '<input id="equationresult" type="tel" value="" size="2" style="border: 1px solid lightgray; outline: none; text-align: center;" onfocus="this.select();" onmouseup="return false;" onkeypress="return clickIfEnter(event, \'equationresultsend\');" />' +
            '&nbsp; <input id="equationresultsend" type="submit" value="&raquo;" onClick="checkResult();"/><br/>' +
            '';
    }
    else {
        var html = 'Det finns ' + m_numItems.toString() + ' tal, hur många vill du göra nu?<br/>' +
            '<input id="eqitNumTasks" type="number" min="1" max="' + m_numItems.toString() + '" value="20"/><br/>' +
            '<input type="radio" name="eqitTimerType" value="0">ingen timer</input><br/>' +
            '<input type="radio" name="eqitTimerType" value="1">ta tiden</input><br/>' +
            '<input type="radio" name="eqitTimerType" value="2">begränsad tid</input> <input id="eqitTimerLimit" type="number" min="1" max="300" /> sekunder<br/>' +
            '<input id="eqitStart" type="button" value="Starta" onClick="eqitStart();"/>';
    }
    div.innerHTML = html;
}

function eqitStart() {
    m_started = true;

    m_numItems = TryParseInt(document.getElementById("eqitNumTasks").value, 20);
    m_timerType = getSelectedRadioValue("eqitTimerType");
    m_timerLimit = TryParseInt(document.getElementById("eqitTimerLimit").value, 0);

    displayEquationIterator();
    displayEquation();

    m_startTime = new Date();
    updateTimer();
}

function displayTimeOut() {
    var div = document.getElementById("equationiterator");
    div.innerHTML = "<br/>Du hann inte riktigt den här gången, men du klarade " + m_currentIndex.toString() + " tal på " + m_timerLimit.toString() + " sekunder. Bra jobbat!" + 
        "<br/><input type='button' value='Tillbaka' onClick='window.location.reload();'/>";
}

function updateTimer() {
    var value = "";
    switch (m_timerType) {
        case "0": 
            return;
        case "1":
            value = getTimeSpent();
            break;
        case "2":
            value = getTimeLeft();
            if (value <= 0) {
                displayTimeOut();
            }
            break;
    }

    document.getElementById("timer").innerHTML = value;
    m_timer = setTimeout(updateTimer, 250);
}

function getTimeSpent() {
    var now = new Date();
    var diff = now - m_startTime;
    var s = ((diff / 1000) % 60) | 0;
    var m = ((diff / 1000) / 60) | 0;

    return pad(m,2) + ":" + pad(s,2);
}

function getTimeLeft() {
    var now = new Date();
    var diff = now - m_startTime;
    var s = (diff / 1000) | 0;
    var timeLeft = m_timerLimit - s;
    return timeLeft;
}

function stopTimer() {
    m_timeSpent = getTimeSpent();
    clearTimeout(m_timer);
}

function checkResult()
{
    //assert its a number

    var div = document.getElementById("equationresult");
    if (div.value === m_model[m_currentIndex].Result) {
        div.style.color = "Green";
        //sleep 500
        setTimeout(function () {
            div.style.color = "Black";
            displayNextEquation();
        }, 750);
    }
    else {
        if (m_incorrectAnswers.indexOf(m_currentIndex) === -1) {
            m_incorrectAnswers[m_incorrectAnswers.length] = m_currentIndex;
        }
        div.style.color = "Red";
        setTimeout(function () {
            div.style.color = "Black";
            displayEquation();
        }, 750);
    }
}

function clickIfEnter(e, buttonId)
{
    // look for window.event in case event isn't passed in
    e = e || window.event;
    if (e.keyCode === 13)
    {
        document.getElementById(buttonId).click();
        return false;
    }
    return true;
}

function TryParseInt(str, defaultValue) {
    var retValue = defaultValue;
    if (str !== null) {
        if (str.length > 0) {
            if (!isNaN(str)) {
                retValue = parseInt(str);
            }
        }
    }
    return retValue;
}

function padWithZero(n, width) {
    return (String(0).repeat(width) + String(n)).slice(String(n).length);
}

function getSelectedRadioValue(groupName) {
    var groupElements = document.getElementsByName(groupName);
    var value;
    for (var i = 0; i < groupElements.length; i++) {
        if (groupElements[i].checked) {
            value = groupElements[i].value;
        }
    }
    return value;
}
