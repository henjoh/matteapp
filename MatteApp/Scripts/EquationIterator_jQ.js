﻿var m_model;
var m_currentIndex;
var m_numItems;
var m_incorrectAnswers = [];
var m_started = false;

//TODO:
//-extract css styles
//-track result (success on first try?) and show when done
//-take time and show timer
//-extract problems?


function initEquationIterator(model, rerun)
{
    m_model = model;
    m_currentIndex = 0;
    m_numItems = m_model.length;
    m_incorrectAnswers = [];
    m_started = rerun || false;

    displayEquationIterator();
}

function displayEquation()
{
    var counterDiv = document.getElementById("equationiteratorcounter");
    counterDiv.innerHTML = (m_currentIndex + 1).toString() + " av " + m_numItems.toString();

    var formulaLabel = document.getElementById("equationiteratorformula");
    formulaLabel.innerHTML = m_model[m_currentIndex].Formula;

    var resultInput = document.getElementById("eqitResult");
    resultInput.value = "";
    resultInput.focus();
    resultInput.select();
}

$("#eqitResult").blur(function () {
    $(this).focus();
    $(this).select();
});

function displayNextEquation()
{
    m_currentIndex++;
    if (m_currentIndex < m_numItems) {
        displayEquation();
    }
    else {
        displayFinished();
    }
}

function displayFinished() {
    var div = document.getElementById("equationiterator");
    div.innerHTML = "<br/>Du klarade det. Bra jobbat!";
    if (m_incorrectAnswers.length > 0) {
        div.innerHTML += "<br/><font size=\"4\">Du gissade fel på " + m_incorrectAnswers.length.toString() + " tal." +
            "<br/><input type='button' value='Kör om de här' onClick='rerunIncorrect();'/>";
    }
    else {
        //TODO: knapp - kör alla igen
    }
    //TODO: knapp - till startsidan
}

function rerunIncorrect()
{
    var newModel = [];
    for (var i = 0; i < m_incorrectAnswers.length; i++) {
        newModel[i] = m_model[m_incorrectAnswers[i]];
    }
    initEquationIterator(newModel, true);
    displayEquation();
}

function displayEquationIterator()
{
    var div = document.getElementById("equationiterator");
    if (m_started === true) {
        div.style = "font-size: 25px;";
        var html = '<div id="equationiteratorcounter"></div><br/>' +
            '<label id="equationiteratorformula"></label> = ' +
            '<input id="eqitResult" type="tel" value="" size="2" style="border: 1px solid lightgray; outline: none; text-align: center;" onfocus="this.select();" onmouseup="return false;" />' +
            '&nbsp; <input id="eqitSend" type="submit" value="&raquo;" onClick="checkResult();"/><br/>' +
            '';
    }
    else {
        var html = 'Antal: <input type="radio" name="eqitnumtasks" value="5"> 5</input> ' +
            '<input type="radio" name="eqitnumtasks" value="20"> 20</input> ' +
            '<input type="radio" name="eqitnumtasks" value="all" checked="1"> Alla</input><br/>' +
            '<input id="eqitStart" type="button" value="Starta" onClick="eqitStart();"/>';
    }
    div.innerHTML = html;
}

$("#eqitStart").click(function (e) {
    m_started = true;

    //get settings
    var numTasks = getSelectedRadioValue("eqitnumtasks");
    if (numTasks != "all") {
        m_numItems = Number(numTasks);
    }

    displayEquationIterator();
    displayEquation();
});

function getSelectedRadioValue(groupName) {
    var groupElements = document.getElementsByName(groupName);
    var value;
    for (var i = 0; i < groupElements.length; i++) {
        if (groupElements[i].checked) {
            value = groupElements[i].value;
        }
    }
    return value;
}

$("#eqitResult").keypress(function () {
    clickIfEnter(event, 'eqitResultsend');
});

$("#eqitSend").click(function (e) {
    e.preventDefault();

    //assert its a number

    var div = document.getElementById("eqitResult");
    if (div.value === m_model[m_currentIndex].Result) {
        div.style.color = "Green";
        //sleep 500
        setTimeout(function () {
            div.style.color = "Black";
            displayNextEquation();
        }, 750);
    }
    else {
        if (m_incorrectAnswers.indexOf(m_currentIndex) === -1) {
            m_incorrectAnswers[m_incorrectAnswers.length] = m_currentIndex;
        }
        div.style.color = "Red";
        setTimeout(function () {
            div.style.color = "Black";
            displayEquation();
        }, 750);
    }
});

function clickIfEnter(e, buttonId)
{
    // look for window.event in case event isn't passed in
    e = e || window.event;
    if (e.keyCode === 13)
    {
        document.getElementById(buttonId).click();
        return false;
    }
    return true;
}
