﻿using MatteApp.Providers;
using System.Web.Mvc;

namespace MatteApp.Controllers
{
    //[Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult SimpleProblems(string id)
        {
            ViewBag.ProblemListName = id;
            return View(SimpleProblemProvider.GetProblems(id));
        }
    }
}
