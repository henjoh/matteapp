﻿using MatteApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatteApp.Providers
{
    public static class SimpleProblemProvider
    {
        #region Lilla Minus

        private static SimpleProblemModel[] lillaMinus = new SimpleProblemModel[] {
            new SimpleProblemModel() { Formula = "1 - 0" , Result =  "1" },
            new SimpleProblemModel() { Formula = "1 - 1" , Result =  "0" },
            new SimpleProblemModel() { Formula = "2 - 0" , Result =  "2" },
            new SimpleProblemModel() { Formula = "2 - 1" , Result =  "1" },
            new SimpleProblemModel() { Formula = "2 - 2" , Result =  "0" },
            new SimpleProblemModel() { Formula = "3 - 0" , Result =  "3" },
            new SimpleProblemModel() { Formula = "3 - 1" , Result =  "2" },
            new SimpleProblemModel() { Formula = "3 - 2" , Result =  "1" },
            new SimpleProblemModel() { Formula = "3 - 3" , Result =  "0" },
            new SimpleProblemModel() { Formula = "4 - 0" , Result =  "4" },
            new SimpleProblemModel() { Formula = "4 - 1" , Result =  "3" },
            new SimpleProblemModel() { Formula = "4 - 2" , Result =  "2" },
            new SimpleProblemModel() { Formula = "4 - 3" , Result =  "1" },
            new SimpleProblemModel() { Formula = "4 - 4" , Result =  "0" },
            new SimpleProblemModel() { Formula = "5 - 0" , Result =  "5" },
            new SimpleProblemModel() { Formula = "5 - 1" , Result =  "4" },
            new SimpleProblemModel() { Formula = "5 - 2" , Result =  "3" },
            new SimpleProblemModel() { Formula = "5 - 3" , Result =  "2" },
            new SimpleProblemModel() { Formula = "5 - 4" , Result =  "1" },
            new SimpleProblemModel() { Formula = "5 - 5" , Result =  "0" },
            new SimpleProblemModel() { Formula = "6 - 0" , Result =  "6" },
            new SimpleProblemModel() { Formula = "6 - 1" , Result =  "5" },
            new SimpleProblemModel() { Formula = "6 - 2" , Result =  "4" },
            new SimpleProblemModel() { Formula = "6 - 3" , Result =  "3" },
            new SimpleProblemModel() { Formula = "6 - 4" , Result =  "2" },
            new SimpleProblemModel() { Formula = "6 - 5" , Result =  "1" },
            new SimpleProblemModel() { Formula = "6 - 6" , Result =  "0" },
            new SimpleProblemModel() { Formula = "7 - 0" , Result =  "7" },
            new SimpleProblemModel() { Formula = "7 - 1" , Result =  "6" },
            new SimpleProblemModel() { Formula = "7 - 2" , Result =  "5" },
            new SimpleProblemModel() { Formula = "7 - 3" , Result =  "4" },
            new SimpleProblemModel() { Formula = "7 - 4" , Result =  "3" },
            new SimpleProblemModel() { Formula = "7 - 5" , Result =  "2" },
            new SimpleProblemModel() { Formula = "7 - 6" , Result =  "1" },
            new SimpleProblemModel() { Formula = "7 - 7" , Result =  "0" },
            new SimpleProblemModel() { Formula = "8 - 0" , Result =  "8" },
            new SimpleProblemModel() { Formula = "8 - 1" , Result =  "7" },
            new SimpleProblemModel() { Formula = "8 - 2" , Result =  "6" },
            new SimpleProblemModel() { Formula = "8 - 3" , Result =  "5" },
            new SimpleProblemModel() { Formula = "8 - 4" , Result =  "4" },
            new SimpleProblemModel() { Formula = "8 - 5" , Result =  "3" },
            new SimpleProblemModel() { Formula = "8 - 6" , Result =  "2" },
            new SimpleProblemModel() { Formula = "8 - 7" , Result =  "1" },
            new SimpleProblemModel() { Formula = "8 - 8" , Result =  "0" },
            new SimpleProblemModel() { Formula = "9 - 0" , Result =  "9" },
            new SimpleProblemModel() { Formula = "9 - 1" , Result =  "8" },
            new SimpleProblemModel() { Formula = "9 - 2" , Result =  "7" },
            new SimpleProblemModel() { Formula = "9 - 3" , Result =  "6" },
            new SimpleProblemModel() { Formula = "9 - 4" , Result =  "5" },
            new SimpleProblemModel() { Formula = "9 - 5" , Result =  "4" },
            new SimpleProblemModel() { Formula = "9 - 6" , Result =  "3" },
            new SimpleProblemModel() { Formula = "9 - 7" , Result =  "2" },
            new SimpleProblemModel() { Formula = "9 - 8" , Result =  "1" },
            new SimpleProblemModel() { Formula = "9 - 9" , Result =  "0" },
            new SimpleProblemModel() { Formula = "10 - 0" , Result =  "10" },
            new SimpleProblemModel() { Formula = "10 - 1" , Result =  "9" },
            new SimpleProblemModel() { Formula = "10 - 2" , Result =  "8" },
            new SimpleProblemModel() { Formula = "10 - 3" , Result =  "7" },
            new SimpleProblemModel() { Formula = "10 - 4" , Result =  "6" },
            new SimpleProblemModel() { Formula = "10 - 5" , Result =  "5" },
            new SimpleProblemModel() { Formula = "10 - 6" , Result =  "4" },
            new SimpleProblemModel() { Formula = "10 - 7" , Result =  "3" },
            new SimpleProblemModel() { Formula = "10 - 8" , Result =  "2" },
            new SimpleProblemModel() { Formula = "10 - 9" , Result =  "1" },
            new SimpleProblemModel() { Formula = "10 - 10" , Result =  "0" },
        };

        #endregion

        #region Lilla Plus

        private static SimpleProblemModel[] lillaPlus = new SimpleProblemModel[] {
            new SimpleProblemModel() { Formula = "1 + 1", Result =  "2" },
            new SimpleProblemModel() { Formula = "2 + 1", Result =  "3" },
            new SimpleProblemModel() { Formula = "3 + 1", Result =  "4" },
            new SimpleProblemModel() { Formula = "4 + 1", Result =  "5" },
            new SimpleProblemModel() { Formula = "5 + 1", Result =  "6" },
            new SimpleProblemModel() { Formula = "6 + 1", Result =  "7" },
            new SimpleProblemModel() { Formula = "7 + 1", Result =  "8" },
            new SimpleProblemModel() { Formula = "8 + 1", Result =  "9" },
            new SimpleProblemModel() { Formula = "9 + 1", Result =  "10" },
            new SimpleProblemModel() { Formula = "1 + 2", Result =  "3" },
            new SimpleProblemModel() { Formula = "2 + 2", Result =  "4" },
            new SimpleProblemModel() { Formula = "3 + 2", Result =  "5" },
            new SimpleProblemModel() { Formula = "4 + 2", Result =  "6" },
            new SimpleProblemModel() { Formula = "5 + 2", Result =  "7" },
            new SimpleProblemModel() { Formula = "6 + 2", Result =  "8" },
            new SimpleProblemModel() { Formula = "7 + 2", Result =  "9" },
            new SimpleProblemModel() { Formula = "8 + 2", Result =  "10" },
            new SimpleProblemModel() { Formula = "1 + 3", Result =  "4" },
            new SimpleProblemModel() { Formula = "2 + 3", Result =  "5" },
            new SimpleProblemModel() { Formula = "3 + 3", Result =  "6" },
            new SimpleProblemModel() { Formula = "4 + 3", Result =  "7" },
            new SimpleProblemModel() { Formula = "5 + 3", Result =  "8" },
            new SimpleProblemModel() { Formula = "6 + 3", Result =  "9" },
            new SimpleProblemModel() { Formula = "7 + 3", Result =  "10" },
            new SimpleProblemModel() { Formula = "1 + 4", Result =  "5" },
            new SimpleProblemModel() { Formula = "2 + 4", Result =  "6" },
            new SimpleProblemModel() { Formula = "3 + 4", Result =  "7" },
            new SimpleProblemModel() { Formula = "4 + 4", Result =  "8" },
            new SimpleProblemModel() { Formula = "5 + 4", Result =  "9" },
            new SimpleProblemModel() { Formula = "6 + 4", Result =  "10" },
            new SimpleProblemModel() { Formula = "1 + 5", Result =  "6" },
            new SimpleProblemModel() { Formula = "2 + 5", Result =  "7" },
            new SimpleProblemModel() { Formula = "3 + 5", Result =  "8" },
            new SimpleProblemModel() { Formula = "4 + 5", Result =  "9" },
            new SimpleProblemModel() { Formula = "5 + 5", Result =  "10" },
            new SimpleProblemModel() { Formula = "1 + 6", Result =  "7" },
            new SimpleProblemModel() { Formula = "2 + 6", Result =  "8" },
            new SimpleProblemModel() { Formula = "3 + 6", Result =  "9" },
            new SimpleProblemModel() { Formula = "4 + 6", Result =  "10" },
            new SimpleProblemModel() { Formula = "1 + 7", Result =  "8" },
            new SimpleProblemModel() { Formula = "2 + 7", Result =  "9" },
            new SimpleProblemModel() { Formula = "3 + 7", Result =  "10" },
            new SimpleProblemModel() { Formula = "1 + 8", Result =  "9" },
            new SimpleProblemModel() { Formula = "2 + 8", Result =  "10" },
            new SimpleProblemModel() { Formula = "1 + 9", Result =  "10" }
        };

        #endregion

        public static SimpleProblemModel[] GetProblems(string name)
        {
            List<SimpleProblemModel> list;

            switch (name)
            {
                case "Lilla Minus":
                    list = lillaMinus.ToList();
                    break;
                case "Lilla Plus":
                    list = lillaPlus.ToList();
                    break;
                default:
                    throw new ArgumentException("Unknown problem list name!", "name");
            }

            Random random = new Random();
            list.Sort((x, y) => random.Next(-1, 1));
            return list.ToArray();
        }
    }
}