﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatteApp.Models
{
    public class SimpleProblemModel
    {
        public string Formula { get; set; }

        public string Result { get; set; }
    }
}